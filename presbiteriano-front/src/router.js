import Vue from 'vue'
import Router from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import vbclass from 'vue-body-class'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/nossa-historia',
      name: 'history',
      component: () => import('./views/HistoryView.vue'),
    },
    {
      path: '/sistema-mackenzie',
      name: 'sistema-mackenzie',
      component: () => import('./views/SistemaMackenzieView.vue'),
    },
    {
      path: '/says',
      name: 'says',
      component: () => import('./views/SaysView.vue'),
    },
    {
      path: '/equipe',
      name: 'team',
      meta: { bodyClass: 'bg-green' },
      component: () => import('./views/TeamView.vue'),
    },
    {
      path: '/comunicados',
      name: 'comunicados',
      component: () => import('./views/ComunicacoesView.vue'),
    },
    {
      path: '/segmentos',
      name: 'segments',
      meta: { bodyClass: 'bg-green' },
      component: () => import('./views/SegmentsView.vue'),
    },
    {
      path: '/projetos-pedagogicos',
      name: 'projects',
      component: () => import('./views/ProjectsView.vue'),
    },
    {
      path: '/depoimentos',
      name: 'testimonials',
      meta: { bodyClass: 'bg-green' },
      component: () => import('./views/TestimonialsView.vue'),
    },
    {
      path: '/galeria',
      name: 'galleries',
      component: () => import('./views/GalleriesView.vue'),
    },
    {
      path: '/galeria/:categorySlug',
      name: 'gallery',
      component: () => import('./views/GalleryView.vue'),
    },
  ],
  scrollBehavior (to, from, savedPosition) {

      if (to.hash) {
        return window.scrollTo({
          top: document.querySelector(to.hash).offsetTop,
          behavior: 'smooth'
        })
      }

    return { x: 0, y: 0 }
  }
})

Vue.use( vbclass, router )
export default router;
