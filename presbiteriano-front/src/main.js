import Vue from 'vue'
import Index from './Index.vue'
import router from './router'
import store from './store'
import { mapActions } from 'vuex'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebookF, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faPencilAlt, faBars, faTimes, faEnvelope} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueAwesomeSwiper from 'vue-awesome-swiper'

Vue.config.productionTip = false
library.add(faTimes)

library.add(faFacebookF, faInstagram, faPencilAlt, faBars, faEnvelope)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(VueAwesomeSwiper)

new Vue({
  router,
  store,
  created() {
    this.getPageData();
  },
  methods: {
    ...mapActions('commonData', ['getPageData',]),
  },
  render: h => h(Index),
}).$mount('#app')
