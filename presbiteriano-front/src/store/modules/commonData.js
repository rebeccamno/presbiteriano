import HomeService from '@/services/HomeService'

const homeService = HomeService.build()

const state = {
  pageData: {}
}

const actions = {
  getPageData ({state, commit }) {
    homeService.list()
    .then(
        response => {
          commit('setPageData', response)
        },
        error => {
            console.log("ERROR");
        }
    );
  },
}

const mutations = {

  setPageData(state, pageData) {
    state.pageData = pageData
  }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
