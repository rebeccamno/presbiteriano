import Vue from 'vue'
import Vuex from 'vuex'
import commonData from './modules/commonData'
import createPersistedState from 'vuex-persistedstate'


Vue.use(Vuex)


export default new Vuex.Store({
  modules: {
    commonData
  },
  plugins: [createPersistedState()],
})
