import Api from '@/services/Api'

export default class HomeService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'home/'
    }
}
