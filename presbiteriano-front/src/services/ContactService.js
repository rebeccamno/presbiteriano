import Api from '@/services/Api'

export default class ContactService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'priv/api/contato/'
    }
    sendContactMessage(data) {
      return this.instance.post(this.resource, data)
      .then(this.constructor.then)
      .catch( (err) => {
        console.log(err);
      })
    }
}
