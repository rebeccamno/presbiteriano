import Api from '@/services/Api'

export default class TeamService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'equipe/'
    }
}
