import Api from '@/services/Api'

export default class SegmentsService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'segmentos/'
    }
}
