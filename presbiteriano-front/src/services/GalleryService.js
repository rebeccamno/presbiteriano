import Api from '@/services/Api'

export default class GalleryService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'galerias/'
    }
    getBySlug(slug) {
      return this.instance.get(this.API_url + this.resource + slug)
      .then(this.constructor.then)
      .catch( (err) => {
        console.log(err);
      })

    }
}
