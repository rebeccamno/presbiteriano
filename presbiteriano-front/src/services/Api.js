import Service from '@/services/Service';
import axios from "axios";



const authHeader = function () {

  let user = JSON.parse(localStorage.getItem('user'));
  return user ? `Token ${user.token}` : ''
}


const instance = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  timeout: 10000,
});

instance.interceptors.request.use (
  function (config) {
    config.headers.Authorization = authHeader();
    return config;
  },
  function (error) {
    return Promise.reject (error);
  }
);

/**
 * @type {Api}
 */
export default class Api extends Service {
  /**
   * @param {Object} options
   * @param {string} resource
   */
  constructor (options, resource) {
    super(options)
    this.resource = resource
    this.instance = instance
    this.API_url = 'priv/api/'
  }

  /**
   * @param {string} id
   */
  list() {
    return this.instance.get(this.API_url + this.resource)
    .then(this.constructor.then)
    .catch( (err) => {
      console.log(err);
    })

  }


  static then (response) {
    if (!response.data) {
      return {}
    }
    if (typeof response.data === 'string') {
      return JSON.parse(response.data)
    }
    return response.data
  }
}
