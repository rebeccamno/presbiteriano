import Api from '@/services/Api'

export default class TestimonialsService extends Api {
    constructor(options) {
      super(options)
      this.resource = 'depoimentos/'
    }
}
